angular.module('starter.profile', ['starter.config', 'ionic', 'ionicLazyLoad'])


//.controller('LoginCtrl', function($scope, AuthService, $ionicPopup, $state) {

.controller('LoginCtrl', function($scope, AuthService, $ionicPopup, $state) {
  $scope.user = {
    email: '',
    password: ''
  };

  $scope.login = function() {
    AuthService.login($scope.user).then(function(msg) {
      $state.go('kauppa.deals');
    }, function(errMsg) {
      var alertPopup = $ionicPopup.alert({
        title: 'Login failed!',
        template: errMsg
      });
    });
  };
})

.controller('BonusCtrl', function($scope, localStorageUser, localStorageProvince) {
    // todo:
    console.log(window.localStorage.getItem(localStorageProvince))
})


.controller('StorageCtrl', function($scope, localStorageUser, localStorageProvince) {

//        console.log(window.localStorage.getItem(localStorageUser))

        $scope.tokenkauppa = (window.localStorage.getItem('tokenkauppa'))
        $scope.var_1 = localStorageUser
        $scope.localStorageUser = (window.localStorage.getItem(localStorageUser))
        $scope.var_2 = localStorageProvince
        $scope.localStorageProvince = (window.localStorage.getItem(localStorageProvince))
})

.controller('WalletCtrl', function($scope) {
})

.controller('CouponToUseCtrl', function($scope, $ionicLoading, $http, API_ENDPOINT) {

    var url = API_ENDPOINT + 'v1/profile/coupon/to_use/'
    $scope.title = "Miei coupon"

    var token = window.localStorage.getItem('tokenkauppa');
    var headers = { "Authorization": 'Token ' + token };
    $http.get(url, { headers: headers }).success(function (coupon_list) {
        console.log(coupon_list);
        $scope.coupon_list = coupon_list
        $ionicLoading.hide();
    }).error(function(data){
          alert(data);
    });
})

.controller('CouponUsedCtrl', function($scope, $ionicLoading, $http, API_ENDPOINT) {

    var url = API_ENDPOINT + 'v1/profile/coupon/used/'
    $scope.title = "Coupon usati"

    var token = window.localStorage.getItem('tokenkauppa');
    var headers = { "Authorization": 'Token ' + token };
    $http.get(url, { headers: headers }).success(function (coupon_list) {

        console.log(coupon_list);

        $scope.coupon_list = coupon_list
        $ionicLoading.hide();
    }).error(function(data){
          alert(data);
    });
})
