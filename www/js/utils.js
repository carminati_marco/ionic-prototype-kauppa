angular.module('starter.utils', ['starter.config', 'ionic', 'ionicLazyLoad'])


.controller('ProvinceListCtrl', function($scope, $ionicLoading, $http, API_ENDPOINT) {

    console.log('ProvinceListCtrl')
    $ionicLoading.show({template: 'Recupero i territori kauppa'});

    var url = API_ENDPOINT + 'v1/utils/nation/'
    console.log(url)
    $http.get(url, {
      headers: { 'Content-type': 'application/json' }
    }).success(function (nation_list) {

        console.log(nation_list);
        $scope.nation_list = nation_list;
        $ionicLoading.hide();
    }).error(function(data){
          alert(data);
    });
})

.controller('FAQListCtrl', function($scope, $ionicLoading, $http, API_ENDPOINT) {

    console.log('FAQListCtrl')
    $ionicLoading.show({template: 'Recupero le FAQ'});

    var url = API_ENDPOINT + 'v1/utils/faq/'
    console.log(url)
    $http.get(url, {
      headers: { 'Content-type': 'application/json' }
    }).success(function (faq_list) {

        console.log(faq_list);
        $scope.faq_list = faq_list;
        $ionicLoading.hide();
    }).error(function(data){
          alert(data);
    });

      $scope.toggleGroup = function(faq) {
        if ($scope.isGroupShown(faq)) {
          $scope.shownGroup = null;
        } else {
          $scope.shownGroup = faq;
        }
      };
      $scope.isGroupShown = function(faq) {
        return $scope.shownGroup === faq;
      };
})

.controller('AccediCtrl', function($scope, $ionicLoading, $http, API_ENDPOINT) {

    console.log('AccediCtrl')
    $ionicLoading.show({template: 'Recupero le FAQ'});

    var url = API_ENDPOINT + 'v1/utils/nation/'
    console.log(url)
    $http.get(url, {
      headers: { 'Content-type': 'application/json' }
    }).success(function (nation_list) {

        console.log(nation_list);
        $scope.nation_list = nation_list;
        $ionicLoading.hide();
    }).error(function(data){
          alert(data);
    });
})