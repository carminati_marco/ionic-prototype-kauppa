angular.module('starter')
 
.service('AuthService', function($q, $http, API_ENDPOINT) {
  var LOCAL_TOKEN_KEY = 'tokenkauppa';
  var isAuthenticated = false;
  var authToken;
 
  function loadUserCredentials() {
    var token = window.localStorage.getItem(LOCAL_TOKEN_KEY);
    if (token) {
      useCredentials(token);
    }
  }
 
  function storeUserCredentials(token) {
    window.localStorage.setItem(LOCAL_TOKEN_KEY, token);
    useCredentials(token);
  }
 
  function useCredentials(token) {
    isAuthenticated = true;
    authToken = token;
 
    // Set the token as header for your requests!
    $http.defaults.headers.common.Authorization = authToken;
  }
 
  function destroyUserCredentials() {
    authToken = undefined;
    isAuthenticated = false;
    $http.defaults.headers.common.Authorization = undefined;
    window.localStorage.removeItem(LOCAL_TOKEN_KEY);
  }
 
  var register = function(user) {
    return $q(function(resolve, reject) {
      $http.post(API_ENDPOINT.url + '/auth/signup/', user).then(function(result) {
        if (result.data.succes) {
          resolve(result.data.msg);
        } else {
          reject(result.data.msg);
        }
      });
    });
  };
 
  var login = function(user) {
    return $q(function(resolve, reject) {
        console.log('qui >>>>')
        console.log(user)
        $http.post(API_ENDPOINT + 'auth/login/', user).then(function(result) {
        if (result.data.auth_token) {
          storeUserCredentials(result.data.auth_token);
          resolve(result.data.msg);
        } else {
          reject(result.data.msg);
        }
      });
    });
  };
 
  var logout = function() {
    destroyUserCredentials();
  };
 
  loadUserCredentials();
 
  return {
    login: login,
    register: register,
    logout: logout,
    isAuthenticated: function() {return isAuthenticated;},
  };
})


.service('DealService', function($q, $http, API_ENDPOINT) {

  var get_user_data = function(token) {

    return $q(function(resolve, reject) {
        var headers = { "Authorization": 'Token ' + token };
        $http.get(API_ENDPOINT + 'v1/profile/profile-data/', {
            headers: headers
          }).then(function(result) {
            if (result.data.pk) {
                console.log('SALVO USER')
                console.log(result.data.pk)
                window.localStorage.setItem('USER', result.data.pk);
                return result.data.pk;
            } else {
                window.localStorage.removeItem('USER');
            }
        });
    });

//    console.log('get_user_data');
//    console.log(token);
//
//    var headers = { "Authorization": 'Token ' + token };
//    $http.get(API_ENDPOINT + '/v1/profile/profile-data/', {
//        headers: headers
//      }).then(function(result) {
//        if (result.data.pk) {
//            console.log('SALVO USER')
//            window.localStorage.setItem('USER', result.data);
//            return result.data;
//        } else {
//            window.localStorage.removeItem(LOCAL_TOKEN_KEY);
//        }
//    });
  };

  var get_province_data = function(selected_province_id) {
    console.log('get_province_data');

    var url_province = API_ENDPOINT + 'v1/utils/province-data/?province_id=' + selected_province_id
    $http.get(url_province).success(function (data) {
        console.log('data');
        console.log(data)
        window.localStorage.setItem("pk", data.pk)
        window.localStorage.setItem("name", data.name)
        window.localStorage.setItem("nation_code", data.nation_code)
        window.localStorage.setItem("nation_name", data.nation_name)
        window.localStorage.setItem("latitude", data.latitude)
        window.localStorage.setItem("longitude", data.longitude)
        window.localStorage.setItem("special_group", data.special_group)
        window.localStorage.setItem("has_on_tv", data.has_on_tv)

//        $rootScope.name = data.name;
//        $rootScope.special_groups = data.special_group;
//        $rootScope.has_on_tv = data.has_on_tv;

        return data.name, data.special_groups, data.has_on_tv

    }).error(function(data){
          alert(data);
    });
  };

  // ritorno le function dichiarate
  return {
    get_user_data: get_user_data,
    get_province_data: get_province_data
  }
})