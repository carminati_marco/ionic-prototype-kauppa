angular.module('starter.kauppa', ['starter.config', 'ionic', 'ionicLazyLoad'])


.controller('DealListCtrl', function($scope, $rootScope, $ionicLoading, $http, $stateParams, API_ENDPOINT,
                                     localStorageUser, localStorageProvince, DealService) {
    // init app controller.
    var token = window.localStorage.getItem('tokenkauppa');  // get user token.
    if (token) {
        // setup var token in rootScope (used in other views.)
        $rootScope.profile = window.localStorage.getItem(localStorageUser)
        if ($rootScope.profile) {
            $rootScope.profile = JSON.parse($rootScope.profile)
        } else {
        var headers = { "Authorization": 'Token ' + token };
            $http.get(API_ENDPOINT + 'v1/profile/profile-data/', {
                headers: headers
              }).then(function(result) {

                if (result.data.pk) {
                    $rootScope.profile = result.data
                    window.localStorage.setItem(localStorageUser, JSON.stringify(result.data));
                } else {
                    window.localStorage.removeItem(localStorageUser);
                }
            });
        }
    } else {
        window.localStorage.removeItem(localStorageUser);
    }

    // init province section.
    var selected_province_id = $stateParams.id
    var province_id = window.localStorage.getItem('province_id')
    $ionicLoading.show({
        template: 'Recupero offerte'
    });

    // setup province per localStorage.
    if (typeof(selected_province_id) != "undefined") {
        if (selected_province_id != $rootScope.province_id) {
            $rootScope.province_id = selected_province_id
        }
    } else {
        if (!$rootScope.province_id) {
            $rootScope.province_id = window.localStorage.getItem('province_id')
            if (typeof($rootScope.province_id) != "undefined") {
                $rootScope.province_id = 13
            }
        }
        selected_province_id = $rootScope.province_id
    }

    // save in localStorage + call 'view list-province'
    window.localStorage.setItem('province_id', selected_province_id)
    var url_province = API_ENDPOINT + 'v1/utils/province-data/?province_id=' + selected_province_id
    $http.get(url_province, {}).then(function(result) {
        window.localStorage.setItem(localStorageProvince, JSON.stringify(result.data));
        $rootScope.province = result.data
    });

    // call view to obtain deal list.
    var url = API_ENDPOINT + 'v1/kauppa/deal/?province_id=' + $rootScope.province_id;
    $http.get(url, {
      headers: { 'Content-type': 'application/json' }
    }).success(function (deals) {
        $scope.deals = deals;
        $ionicLoading.hide();
    }).error(function(){
          alert(error);
    });

})

.controller('DealTVListCtrl', function($scope, $rootScope, $ionicLoading, $http, API_ENDPOINT) {
    // Controller Deal TV.
    $ionicLoading.show({template: 'Recupero offerte TV'});  // loading.

    var province_id = window.localStorage.getItem('province_id')
    var url = API_ENDPOINT + 'v1/kauppa/deal-tv/?province_id=' + province_id

    $http.get(url, {
      headers: { 'Content-type': 'application/json' }
    }).success(function (deals) {
        $scope.deals = deals;
        $ionicLoading.hide();
    }).error(function(data){
          alert(data);
    });
})


.controller('MapDealListCtrl', function($scope, $rootScope, $ionicLoading, $http, API_ENDPOINT) {
    // Controller Map View.
    $ionicLoading.show({template: 'Recupero offerte'});  // Loading.

    // get latitude + longitude per maps.
    $rootScope.latitude = window.localStorage.getItem("latitude")
    $rootScope.longitude = window.localStorage.getItem("longitude")

    var province_id = window.localStorage.getItem('province')
    var url = API_ENDPOINT + 'v1/kauppa/deal-map/'
    $http.get(url, {
      headers: { 'Content-type': 'application/json' }
    }).success(function (map_deals) {
        $scope.map_deals = map_deals;
        $ionicLoading.hide();
    }).error(function(data){
          alert(data);
    });
})


.controller('DealSpecialListCtrl', function($scope, $rootScope, $ionicLoading, $stateParams, $http, API_ENDPOINT) {
    // Controller Map View.
    $ionicLoading.show({template: 'Recupero offerte Speciale'});  // Loading.

    var special_group_id = $stateParams.special_group_id
    var url = API_ENDPOINT + 'v1/kauppa/deal-special/' + special_group_id

    $http.get(url, {
      headers: { 'Content-type': 'application/json' }
    }).success(function (deals) {
        $scope.deals = deals;
        $ionicLoading.hide();
    }).error(function(data){
          alert(data);
    });
})


.controller('DealCtrl', function($scope, $ionicLoading, $ionicPopup, $stateParams,  $http, API_ENDPOINT) {
    // Controller Detail Deal.
    $ionicLoading.show({template: 'Recupero offerta'});  // Loading.

    var url = API_ENDPOINT + 'v1/kauppa/deal/' + $stateParams.id +'/?format=json';

    $http.get(url, {
      headers: {
        'Content-type': 'application/json'
      }
    }).success(function (deal) {
        $scope.deal = deal;
        $ionicLoading.hide();
    });
})
