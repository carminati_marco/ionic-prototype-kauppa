// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers',
        'starter.kauppa',
        'starter.profile',
        'starter.utils',
        'ngMap'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })

  .state('app.browse', {
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html'
        }
      }
    })


  .state('kauppa', {
    url: '/kauppa',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('kauppa.deals', {
      cache: false,
      url: '/deals',
      views: {
        'menuContent': {
          templateUrl: 'templates/kauppa/deal_list.html',
          controller: 'DealListCtrl'
        }
      }
    })

  .state('kauppa.deals_province', {
      cache: false,
      url: '/deals/:id',
      views: {
        'menuContent': {
          templateUrl: 'templates/kauppa/deal_list.html',
          controller: 'DealListCtrl'
        }
      }
    })

  .state('kauppa.deals_tv', {
      url: '/deals_tv',
      views: {
        'menuContent': {
          templateUrl: 'templates/kauppa/deal_list.html',
          controller: 'DealTVListCtrl'
        }
      }
    })

    .state('kauppa.deals_map', {
      cache: false,
      url: '/deals_map',
      views: {
        'menuContent': {
          templateUrl: 'templates/kauppa/map_deal_list.html',
          controller: 'MapDealListCtrl'
        }
      }
    })

  .state('kauppa.deals_special', {
      url: '/deals_special/:special_group_id',
      views: {
        'menuContent': {
          templateUrl: 'templates/kauppa/deal_list.html',
          controller: 'DealSpecialListCtrl'
        }
      }
    })

  .state('kauppa.dealsingle', {
    url: "/deal/:id",
    views: {
      'menuContent': {
        templateUrl: 'templates/kauppa/deal_detail.html',
        controller: 'DealCtrl'
      }
    }
  })

  .state('utils', {
    url: '/utils',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })


    .state('utils.provinces', {
      url: '/provinces',
      views: {
        'menuContent': {

          templateUrl: 'templates/utils/province_list.html',
          controller: 'ProvinceListCtrl'
        }
      }
    })

    .state('utils.faq', {
      url: '/faq',
      views: {
        'menuContent': {
          templateUrl: 'templates/utils/faq_list.html',
          controller: 'FAQListCtrl'
        }
      }
    })

     .state('profile', {
        url: '/profile',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
      })
    .state('profile.login', {
      url: '/login',
      views: {
        'menuContent': {
          templateUrl: 'templates/profile/login.html',
          controller: 'LoginCtrl'
        }
      }
    })
    .state('profile.bonus', {
      url: '/bonus',
      views: {
        'menuContent': {
          templateUrl: 'templates/profile/bonus.html',
          controller: 'BonusCtrl'
        }
      }
    })
    .state('profile.wallet', {
      url: '/wallet',
      views: {
        'menuContent': {
          templateUrl: 'templates/profile/wallet.html',
          controller: 'WalletCtrl'
        }
      }
    })

    .state('profile.storage', {
      url: '/storage',
      views: {
        'menuContent': {
          templateUrl: 'templates/profile/storage.html',
          controller: 'StorageCtrl'
        }
      }
    })
    .state('profile.coupon_used', {
      url: '/coupon_used',
      views: {
        'menuContent': {
          templateUrl: 'templates/profile/coupon_list.html',
          controller: 'CouponUsedCtrl'
        }
      }
    })
    .state('profile.coupon_to_use', {
      url: '/coupon_to_use',
      views: {
        'menuContent': {
          templateUrl: 'templates/profile/coupon_list.html',
          controller: 'CouponToUseCtrl'
        }
      }
    })
  ;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/kauppa/deals');
});
