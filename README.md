# README - this is a prototype #

Prototype created to test the framework ionic using kauppa's API.
Prototipo di app sviluppata in Ionic (http://ionicframework.com) per Kauppa (http://www.kauppa.it/).

### What is this repository? ###

Sample code for ionic to test:

* how to use authentication via API Rest/token.
* how to obtain data via REST
* how to use localization in a MapView
* how to load image when it's visibile

### How do I get set up? ###

* Download the project
* Install ionic http://ionicframework.com/docs/v1/guide/installation.html
* Start!

### Who do I talk to? ###

* Marco Carminati carminati.marco@gmail.com